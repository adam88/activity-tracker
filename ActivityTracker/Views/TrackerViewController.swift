//
//  TrackerViewController.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class TrackerViewController: UIViewController {
    lazy var mapView: MapView = {
        let view = MapView()
        return view
    }()
    
    lazy var tabBarView: TabBarView = {
        let view = TabBarView()
        view.backgroundColor = UIColor.Assets.backgroundSecondary
        view.addTabBarItem(TabBarItem(title: Strings.trackerLocationsIncluded))
        view.addTabBarItem(TabBarItem(title: Strings.trackerLocationsExcluded))
        view.selectedIndex = 1
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.Assets.backgroundPrimary
        view.register(LocationCell.self, forCellReuseIdentifier: LocationCell.identifier)
        view.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 16.0, right: 0.0)
        view.bounces = false
        view.separatorStyle = .none
        return view
    }()
    
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.contentInsetAdjustmentBehavior = .never
        view.bounces = false
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        return view
    }()
    
    private var preferredTabBarHeight: CGFloat {
        return statusBarHeight + 88.0
    }
    
    private let viewModel: TrackerViewModelType
    
    let disposeBag = DisposeBag()
    
    required init(viewModel: TrackerViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        setupBindings()
        setupScrollViewDelegates()
    }
    
    private func setupConstraints() {
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(mapView)
        stackView.addArrangedSubview(tabBarView)
        stackView.addArrangedSubview(tableView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        stackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }
        
        mapView.snp.makeConstraints { (make) in
            make.height.equalTo(view.snp.height).multipliedBy(0.6)
        }
        
        tabBarView.snp.makeConstraints { (make) in
            make.top.equalTo(mapView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(preferredTabBarHeight)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(tabBarView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(view.snp.height).offset(-preferredTabBarHeight)
        }
    }
    
    private func setupBindings() {
        mapView.navigateButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.mapView.naviagte()
            })
            .disposed(by: disposeBag)
        
        mapView.animateButton.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.mapView.animatePath()
            })
            .disposed(by: disposeBag)
        
        tabBarView.rx.selectedIndex
            .bind(to: viewModel.selectedTabIndex)
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Location.self)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.coordinatorDelegate?.didTapLocation($0, from: self)
            })
            .disposed(by: disposeBag)
          
        viewModel.mapDataSource
            .asDriver(onErrorJustReturn: [])
            .drive(onNext: { [weak self] in
                self?.mapView.drawPath(locations: $0)
            })
            .disposed(by: disposeBag)
        
        viewModel.mapDataSource
            .take(1)
            .delay(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.mapView.naviagte()
            })
            .disposed(by: disposeBag)
        
        viewModel.tableDataSource
            .asDriver(onErrorJustReturn: [])
            .drive(tableView.rx.items(cellIdentifier: LocationCell.identifier, cellType: LocationCell.self)) { [weak self] (_, model, cell) in
                self?.configure(cell: cell, model: model)
            }
            .disposed(by: disposeBag)
    }
    
    private func configure(cell: LocationCell, model: Location) {
        cell.titleLabel.text = "\(model)"
    }
}
