//
//  MapView.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import GoogleMaps

class MapView: UIView {
    lazy var mapView: GMSMapView = {
        let view = GMSMapView()
        view.mapType = .hybrid
        return view
    }()
    
    lazy var navigateButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.Assets.accent
        button.tintColor = UIColor.white
        button.setImage(UIImage.Assets.iconNavigation, for: .normal)
        button.alpha = 0.9
        return button
    }()
    
    lazy var animateButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.Assets.accent
        button.tintColor = UIColor.white
        button.setImage(UIImage.Assets.iconPath, for: .normal)
        button.alpha = 0.9
        return button
    }()
    
    private let polylineAnimator = PolylineAnimator()
    
    private var locations: [Location]?
    
    init() {
        super.init(frame: .zero)
        
        addSubview(mapView)
        addSubview(navigateButton)
        addSubview(animateButton)
        
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        navigateButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(44.0)
            make.right.equalToSuperview().inset(16.0)
        }
        
        animateButton.snp.makeConstraints { (make) in
            make.top.equalTo(navigateButton.snp.bottom).offset(12.0)
            make.width.height.equalTo(44.0)
            make.right.equalToSuperview().inset(16.0)
            make.bottom.equalToSuperview().inset(16.0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        navigateButton.layer.cornerRadius = navigateButton.bounds.size.width / 2.0
        animateButton.layer.cornerRadius = animateButton.bounds.size.width / 2.0
    }
    
    func clear() {
        mapView.clear()
    }
    
    func drawPath(locations: [Location]) {
        polylineAnimator.cancel()
        mapView.clear()
        let polyline = GMSPolyline.build(locations: locations)
        polyline.map = mapView
        self.locations = locations
    }
    
    func animatePath() {
        guard let locations = self.locations else {
            return
        }
        mapView.clear()
        polylineAnimator.animate(locations: locations, duration: 2.0, mapView: mapView)
    }
    
    func naviagte() {
        guard let locations = self.locations, locations.count > 0 else {
            return
        }
        
        let latitudes = locations.map({ $0.latitude })
        let longitudes = locations.map({ $0.longitude })
        
        let maxLat = latitudes.max()!
        let minLat = latitudes.min()!
        let maxLong = longitudes.max()!
        let minLong = longitudes.min()!
        
        var region = GMSVisibleRegion()
        region.nearLeft = CLLocationCoordinate2D(latitude: minLat, longitude: minLong)
        region.farRight = CLLocationCoordinate2D(latitude: maxLat, longitude: maxLong)
        let bounds = GMSCoordinateBounds(coordinate: region.nearLeft, coordinate: region.farRight)
        let camera = mapView.camera(for: bounds, insets: .zero)!
        mapView.camera = camera
    }
}
