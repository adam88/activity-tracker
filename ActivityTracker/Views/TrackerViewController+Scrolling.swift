//
//  TrackerViewController+Scrolling.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

extension TrackerViewController: UIScrollViewDelegate {
    var firstSectionHeight: CGFloat {
        return mapView.bounds.size.height
    }
    
    var secondSectionHeight: CGFloat {
        return stackView.bounds.size.height
    }
    
    var parentScrollView: UIScrollView {
        return scrollView
    }
    
    var childScrollView: UIScrollView {
        return tableView
    }
    
    func setupScrollViewDelegates() {
        parentScrollView.isScrollEnabled = true
        childScrollView.isScrollEnabled = false
        
        parentScrollView.rx.didScroll
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.scrollViewDidScroll(self.parentScrollView)
            })
            .disposed(by: disposeBag)
        
        parentScrollView.rx.didEndDragging
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.scrollViewDidEndDragging(self.parentScrollView, willDecelerate: $0)
            })
            .disposed(by: disposeBag)
        
        childScrollView.rx.didScroll
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.scrollViewDidScroll(self.childScrollView)
            })
            .disposed(by: disposeBag)
        
        childScrollView.rx.didEndDragging
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.scrollViewDidEndDragging(self.childScrollView, willDecelerate: $0)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if parentScrollView.contentOffset.y < firstSectionHeight {
            childScrollView.isScrollEnabled = false
        } else {
            childScrollView.isScrollEnabled = true
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == parentScrollView, !decelerate {
            if parentScrollView.contentOffset.y < firstSectionHeight / 2.0 {
                parentScrollView.setContentOffset(.zero, animated: true)
            } else {
                parentScrollView.setContentOffset(CGPoint(x: 0.0, y: firstSectionHeight), animated: true)
            }
        }
    }
}
