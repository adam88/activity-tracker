//
//  TabBarView.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import SnapKit

class TabBarView: UIControl, TabBarItemViewDelegate {
    lazy var indicatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Assets.seperator
        view.layer.cornerRadius = 2.0
        return view
    }()
    
    lazy var bottomLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Assets.seperator
        return view
    }()
    
    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .fill
        view.distribution = .fillEqually
        return view
    }()
    
    weak var delegate: TabBarViewDelegate?
    
    var selectedIndex: Int = 0 {
        didSet {
            setSelectedIndex(selectedIndex, animated: false)
        }
    }
    
    init() {
        super.init(frame: .zero)
        
        addSubview(indicatorView)
        addSubview(stackView)
        addSubview(bottomLineView)
        
        indicatorView.snp.makeConstraints { (make) in
            make.top.equalTo(8.0)
            make.centerX.equalToSuperview()
            make.width.equalTo(36.0)
            make.height.equalTo(4.0)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(88.0)
        }
        
        bottomLineView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(0.5)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addTabBarItem(_ item: TabBarItem) {
        let itemView = TabBarItemView()
        itemView.titleLabel.text = item.title
        itemView.setIsSelected(false)
        itemView.delegate = self
        stackView.addArrangedSubview(itemView)
    }
    
    private func setSelectedIndex(_ index: Int, animated: Bool) {
        guard let items = stackView.arrangedSubviews as? [TabBarItemView], index < items.count else {
            return
        }
        items.forEach({ $0.setIsSelected(false) })
        items[index].setIsSelected(true)
    }
    
    // MARK: TabBarItemViewDelegate
    
    func tabBarItemViewDidSelect(_ view: TabBarItemView) {
        guard let items = stackView.arrangedSubviews as? [TabBarItemView], let index = items.firstIndex(of: view) else {
            return
        }
        selectedIndex = index
        setSelectedIndex(index, animated: true)
        sendActions(for: .valueChanged)
        delegate?.tabBarView(self, didSelectIndex: index)
    }
}

protocol TabBarViewDelegate: class {
    func tabBarView(_ view: TabBarView, didSelectIndex selectedIndex: Int)
}

class TabBarItemView: UIView {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 3
        label.textColor = UIColor.Assets.textPrimary
        return label
    }()
    
    lazy var indicatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Assets.accent
        return view
    }()
    
    lazy var button: UIButton = {
        let button =  UIButton()
        button.addTarget(self, action: #selector(didTouchDown), for: .touchDown)
        button.addTarget(self, action: #selector(didTouchUp), for: .touchUpInside)
        button.addTarget(self, action: #selector(didTouchUp), for: .touchUpOutside)
        button.addTarget(self, action: #selector(didTouchUp), for: .touchCancel)
        button.addTarget(self, action: #selector(didSelect), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: TabBarItemViewDelegate?
    
    private let textFontNormalState = UIFont.robotoRegular(16.0)
    
    private let textFontSelectedState = UIFont.robotoBold(16.0)
    
    init() {
        super.init(frame: .zero)
        
        addSubview(titleLabel)
        addSubview(indicatorView)
        addSubview(button)
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        indicatorView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(4.0)
        }
        
        button.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIsSelected(_ isSelected: Bool) {
        indicatorView.isHidden = !isSelected
        titleLabel.font = isSelected ? textFontSelectedState : textFontNormalState
    }
    
    @objc func didTouchDown() {
        alpha = 0.5
    }
    
    @objc func didTouchUp() {
        alpha = 1.0
    }
    
    @objc func didSelect() {
        delegate?.tabBarItemViewDidSelect(self)
    }
}

protocol TabBarItemViewDelegate: class {
    func tabBarItemViewDidSelect(_ view: TabBarItemView)
}

struct TabBarItem {
    var title = ""
}
