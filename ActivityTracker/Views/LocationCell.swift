//
//  LocationCell.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import SnapKit

class LocationCell: UITableViewCell {
    static let identifier = "LocationCell"
    
    lazy var borderView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4.0
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.separator.cgColor
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.robotoRegular(16.0)
        label.textColor = UIColor.Assets.textPrimary
        label.numberOfLines = 10
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(borderView)
        borderView.addSubview(titleLabel)
        
        borderView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(16.0)
            make.top.equalToSuperview().inset(8.0)
            make.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(12.0)
        }
        
        backgroundColor = UIColor.Assets.backgroundPrimary
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
    }
}
