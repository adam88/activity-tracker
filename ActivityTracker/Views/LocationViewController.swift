//
//  LocationViewController.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LocationViewController: UIViewController {
    lazy var borderView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4.0
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.separator.cgColor
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var detailsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.robotoRegular(18.0)
        label.textColor = UIColor.Assets.textPrimary
        label.numberOfLines = 0
        return label
    }()
    
    lazy var closeButton: UIBarButtonItem = {
        let button = UIBarButtonItem()
        button.image = UIImage.Assets.iconBack
        return button
    }()
    
    private let viewModel: LocationViewModelType
    
    private let disposeBag = DisposeBag()
    
    init(viewModel: LocationViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        setupBindings()
        navigationItem.title = Strings.locationTitle
        view.backgroundColor = UIColor.Assets.backgroundPrimary
    }
    
    private func setupConstraints() {
        navigationItem.leftBarButtonItem = closeButton
        view.addSubview(borderView)
        borderView.addSubview(detailsLabel)
        
        borderView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(16.0)
            make.centerY.equalToSuperview()
        }
        
        detailsLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(16.0)
        }
    }
    
    private func setupBindings() {
        viewModel.details
            .asDriver(onErrorJustReturn: "")
            .drive(detailsLabel.rx.text)
            .disposed(by: disposeBag)
        
        closeButton.rx.tap
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.viewModel.coordinatorDelegate?.didTapClose(from: self)
            })
            .disposed(by: disposeBag)
    }
}
