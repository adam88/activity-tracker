//
//  TrackerViewModelType.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

protocol TrackerViewModelType {
    var mapDataSource: Observable<[Location]> { get }
    var tableDataSource: Observable<[Location]> { get }
    var selectedTabIndex: BehaviorSubject<Int> { get }
    var coordinatorDelegate: TrackerViewModelCoordinatorDelegate? { get set }
}
