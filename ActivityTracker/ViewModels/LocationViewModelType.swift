//
//  LocationViewModelType.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

protocol LocationViewModelType {
    var details: Observable<String> { get }
    var didTapClose: PublishSubject<Void> { get }
    var coordinatorDelegate: LocationViewModelCoordinatorDelegate? { get set }
    
    init(location: Location)
}
