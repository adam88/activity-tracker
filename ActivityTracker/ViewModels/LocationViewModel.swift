//
//  LocationViewModel.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

class LocationViewModel: LocationViewModelType {
    let details: Observable<String>
    
    let didTapClose: PublishSubject<Void> = .init()
    
    weak var coordinatorDelegate: LocationViewModelCoordinatorDelegate?
    
    private let disposeBag = DisposeBag()
    
    required init(location: Location) {
        details = .just("\(location)")
    }
}
