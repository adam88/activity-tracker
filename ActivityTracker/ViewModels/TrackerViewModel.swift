//
//  TrackerViewModel.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

class TrackerViewModel: TrackerViewModelType {
    let mapDataSource: Observable<[Location]>
    
    let tableDataSource: Observable<[Location]>
    
    let selectedTabIndex: BehaviorSubject<Int> = .init(value: 1)
    
    weak var coordinatorDelegate: TrackerViewModelCoordinatorDelegate?
    
    private let dataProvider: LocationDataProvider
    
    private let locationSmoothing: LocationSmoothing
    
    private let locationFiltering: LocationFiltering
    
    init(dataProvider: LocationDataProvider, locationFiltering: LocationFiltering, locationSmoothing: LocationSmoothing) {
        self.dataProvider = dataProvider
        
        self.locationFiltering = locationFiltering
        
        self.locationSmoothing = locationSmoothing
        
        let rawLocations = Observable.just(0)
            .observe(on: ConcurrentDispatchQueueScheduler(qos: .userInteractive))
            .flatMap({ [dataProvider] _ in dataProvider.rx_getLocations() })
            .share(replay: 1, scope: .forever)
        
        let filtredLocations = rawLocations
            .flatMap({ [locationFiltering] in locationFiltering.rx_filter(locations: $0) })
            .share(replay: 1, scope: .forever)
        
        let includedLocations = filtredLocations
            .map({ $0.included })
        
        let excludedLocations = filtredLocations
            .map({ $0.excluded })
        
        let processedLocations = includedLocations
            .flatMap({ [locationSmoothing] in locationSmoothing.rx_smooth(locations: $0) })
            .share(replay: 1, scope: .forever)
        
        mapDataSource = Observable.combineLatest(processedLocations, rawLocations, selectedTabIndex)
            .map({ $2 == 0 ? $0 : $1 })
            .observe(on: MainScheduler.instance)
        
        tableDataSource = Observable.combineLatest(includedLocations, excludedLocations, selectedTabIndex)
            .map({ $2 == 0 ? $0 : $1 })
            .observe(on: MainScheduler.instance)
    }
}
