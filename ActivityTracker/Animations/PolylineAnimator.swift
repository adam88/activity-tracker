//
//  PolylineAnimator.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import GoogleMaps

class PolylineAnimator {
    private var locations: [Location]?
    private var iteration: Int?
    private var path: GMSMutablePath?
    private var polyline: GMSPolyline?
    private var timer: Timer?
    private var isCanceled: Bool?
    private weak var mapView: GMSMapView?
    
    func animate(locations: [Location], duration: TimeInterval, mapView: GMSMapView) {
        timer?.invalidate()
        self.locations = locations
        self.mapView = mapView
        self.iteration = 0
        self.path = GMSMutablePath()
        self.polyline = GMSPolyline()
        self.isCanceled = false
        let timeInterval = duration / Double(locations.count)
        self.timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(nextIteration), userInfo: nil, repeats: true)
    }
    
    func cancel() {
        isCanceled = true
    }
    
    @objc func nextIteration() {
        guard let locations = self.locations,
              let iteration = self.iteration,
              iteration < locations.count,
              let path = self.path,
              let polyline = self.polyline,
              let mapView = self.mapView,
              let isCanceled = self.isCanceled,
              isCanceled == false
        else {
            self.timer?.invalidate()
            self.locations = nil
            self.iteration = nil
            self.path = nil
            self.polyline = nil
            self.timer = nil
            self.mapView = nil
            self.isCanceled = nil
            return
        }
        
        path.add(locations[iteration].clCoordinate)
        polyline.path = path
        polyline.strokeColor = UIColor.Assets.accent
        polyline.strokeWidth = 3.0
        polyline.map = mapView
        self.iteration = iteration + 1
    }
}
