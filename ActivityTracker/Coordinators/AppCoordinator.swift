//
//  AppCoordinator.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    let window: UIWindow?
    var childCoordinators = [Coordinator]()
    weak var delegate: CoordinatorDelegate?
    
    init(window: UIWindow?) {
        self.window = window
    }

    func start() {
        guard let window = window else {
            return
        }
        
        let rootViewController = UINavigationController()
        rootViewController.setNavigationBarHidden(true, animated: false)
        
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        
        let trackerCoordinator = TrackerCoordinator(rootViewController: rootViewController)
        trackerCoordinator.delegate = self
        addChildCoordinator(trackerCoordinator)
        trackerCoordinator.start()
    }

    func finish() {

    }
}

extension AppCoordinator: CoordinatorDelegate {
    func didFinish(from coordinator: Coordinator) {
        removeChildCoordinator(coordinator)
    }
}
