//
//  Coordinator.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var delegate: CoordinatorDelegate? { get set }
    
    func start()
    func finish()
}

extension Coordinator {
    func addChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func removeChildCoordinator(_ coordinator: Coordinator) {
        guard let index = childCoordinators.firstIndex(where: { $0 === coordinator }) else {
            return
        }
        childCoordinators.remove(at: index)
    }

    func removeAllChildCoordinatorsWithType<T: Coordinator>(_ type: T.Type) {
        childCoordinators = childCoordinators.filter { $0 is T  == false }
    }

    func removeAllChildCoordinators() {
        childCoordinators.removeAll()
    }
}

protocol CoordinatorDelegate: class {
    func didFinish(from coordinator: Coordinator)
}
