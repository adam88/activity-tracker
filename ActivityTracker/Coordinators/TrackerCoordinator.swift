//
//  TrackerCoordinator.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

class TrackerCoordinator: Coordinator {
    let rootViewController: UINavigationController
    var childCoordinators = [Coordinator]()
    weak var delegate: CoordinatorDelegate?
    
    init(rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
    }
    
    func start() {
        showTrackerView()
    }
    
    func finish() {
        rootViewController.viewControllers = []
        delegate?.didFinish(from: self)
    }
}

extension TrackerCoordinator {
    func showTrackerView() {
        let vm = TrackerViewModel(dataProvider: LocationFileDataProvider(dataReader: FileDataReader()),
                                  locationFiltering: GPSFilter(),
                                  locationSmoothing: KalmanFilter())
        vm.coordinatorDelegate = self
        let vc = TrackerViewController(viewModel: vm)
        rootViewController.pushViewController(vc, animated: false)
    }
    
    func showLocationView(_ location: Location, from controller: UIViewController) {
        let vm = LocationViewModel(location: location)
        vm.coordinatorDelegate = self
        let vc = LocationViewController(viewModel: vm)
        rootViewController.pushViewController(vc, animated: true)
        rootViewController.setNavigationBarHidden(false, animated: true)
    }
}

protocol TrackerViewModelCoordinatorDelegate: class {
    func didTapLocation(_ location: Location, from controller: UIViewController)
}

extension TrackerCoordinator: TrackerViewModelCoordinatorDelegate {
    func didTapLocation(_ location: Location, from controller: UIViewController) {
        showLocationView(location, from: controller)
    }
}

protocol LocationViewModelCoordinatorDelegate: class {
    func didTapClose(from controller: UIViewController)
}

extension TrackerCoordinator: LocationViewModelCoordinatorDelegate {
    func didTapClose(from controller: UIViewController) {
        self.rootViewController.setNavigationBarHidden(true, animated: false)
        self.rootViewController.popViewController(animated: true)
    }
}
