//
//  UIViewController+StatusBar.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

extension UIViewController {
    var statusBarHeight: CGFloat {
        let window = UIApplication.shared.windows.filter({ $0.isKeyWindow}).first
        return window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    }
}
