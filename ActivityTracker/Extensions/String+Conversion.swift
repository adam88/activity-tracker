//
//  String+Conversion.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

enum StringConversionError: Error {
    case isNotDoubleType(String)
}

extension String {
    func toDouble() throws -> Double {
        guard let value = Double(self) else {
            throw StringConversionError.isNotDoubleType(self)
        }
        return value
    }
}
