//
//  Location+CL.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import CoreLocation

extension Location {
    var clCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude,
                                      longitude: longitude)
    }
    
    var clLocation: CLLocation {
        return CLLocation(coordinate: clCoordinate,
                          altitude: altitude,
                          horizontalAccuracy: accuracy,
                          verticalAccuracy: 0.0,
                          timestamp: Date(timeIntervalSince1970: timestamp))
    }
}
