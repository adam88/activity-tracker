//
//  Polyline+Builder.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSPolyline {
    static func build(locations: [Location]) -> GMSPolyline {
        let path = GMSMutablePath()
        locations.forEach({ path.add($0.clCoordinate) })
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.Assets.accent
        polyline.strokeWidth = 3.0
        return polyline
    }
}
