//
//  LocationDataProvider+Rx.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

extension LocationDataProvider {
    func rx_getLocations() -> Observable<[Location]> {
        return .create { [weak self] observer in
            guard let self = self else {
                return Disposables.create()
            }
            do {
                observer.onNext(try self.getLocations())
                observer.onCompleted()
            } catch {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
}
