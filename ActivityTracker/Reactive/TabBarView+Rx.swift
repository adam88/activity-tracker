//
//  TabBarView+Rx.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: TabBarView {
    var selectedIndex: ControlProperty<Int> {
        return base.rx.controlProperty(
            editingEvents: .valueChanged,
            getter: { (view) in
                view.selectedIndex
            },
            setter: { (view, value) in
                view.selectedIndex = value
            }
        )
    }
}
