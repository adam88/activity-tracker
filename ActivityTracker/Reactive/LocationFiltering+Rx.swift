//
//  LocationFiltering+Rx.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

extension LocationFiltering {
    func rx_filter(locations: [Location]) -> Observable<(included: [Location], excluded: [Location])> {
        return .create { [weak self] observer in
            guard let self = self else {
                return Disposables.create()
            }
            observer.onNext(self.filter(locations: locations))
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
