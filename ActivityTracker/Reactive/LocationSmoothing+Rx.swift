//
//  LocationSmoothing+Rx.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation
import RxSwift

extension LocationSmoothing {
    func rx_smooth(locations: [Location]) -> Observable<[Location]> {
        return .create { [weak self] observer in
            guard let self = self else {
                return Disposables.create()
            }
            observer.onNext(self.smooth(locations: locations))
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
