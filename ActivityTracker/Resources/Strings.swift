//
//  Strings.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

struct Strings {
    static let trackerLocationsIncluded = "tracker_locations_included".localized
    static let trackerLocationsExcluded = "tracker_locations_excluded".localized
    static let locationTitle = "location_title".localized
}
