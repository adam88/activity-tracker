//
//  Images.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 22/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

extension UIImage {
    struct Assets {
        static let iconPath = UIImage(named: "icon_path")!
        static let iconNavigation = UIImage(named: "icon_navigation")!
        static let iconBack = UIImage(named: "icon_back")!
    }
}
