//
//  Colors.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

extension UIColor {
    struct Assets {
        static let backgroundPrimary = UIColor(named: "background_primary")!
        static let backgroundSecondary = UIColor(named: "background_secondary")!
        static let seperator = UIColor(named: "separator")!
        static let accent = UIColor(named: "accent")!
        static let textPrimary = UIColor(named: "text_primary")!
        
    }
}
