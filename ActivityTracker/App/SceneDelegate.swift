//
//  SceneDelegate.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var coordinator: AppCoordinator!
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }
        window = UIWindow(windowScene: windowScene)
        coordinator = AppCoordinator(window: window)
        coordinator.start()
    }
}
