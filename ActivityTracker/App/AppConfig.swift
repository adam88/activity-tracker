//
//  AppConfig.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import UIKit
import GoogleMaps

class AppConfig {
    private static var googleApiKey: String? {
        return Bundle.main.object(forInfoDictionaryKey: "GoogleApiKey") as? String
    }
    
    static func configure(app: UIApplication) {
        guard let key = googleApiKey, !key.isEmpty else {
            fatalError("Google API key is required. Set the key in the Info.plist file.")
        }
        GMSServices.provideAPIKey(key)
    }
}
