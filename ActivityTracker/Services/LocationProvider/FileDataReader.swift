//
//  FileDataReader.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

class FileDataReader: DataReader {
    func read(fileURL: URL) throws -> Data {
        return try Data(contentsOf: fileURL)
    }
}
