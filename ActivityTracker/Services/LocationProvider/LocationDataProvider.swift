//
//  LocationDataProvider.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

protocol LocationDataProvider: class {
    func getLocations() throws -> [Location]
}

