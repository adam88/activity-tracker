//
//  DataReader.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

protocol DataReader {
    func read(fileURL: URL) throws -> Data
}
