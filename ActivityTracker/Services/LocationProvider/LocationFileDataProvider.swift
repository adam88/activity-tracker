//
//  LocationFileDataProvider.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

class LocationFileDataProvider: LocationDataProvider {
    enum Error: Swift.Error {
        case fileNotFound
    }
    
    private let fileName = "path"
    private let fileExtension = "json"
    private let fileReader: DataReader
    
    init(dataReader: DataReader) {
        self.fileReader = dataReader
    }
    
    func getLocations() throws -> [Location] {
        print("LocationFileDataProvider - getLocations")
        guard let fileURL = Bundle.main.url(forResource: fileName, withExtension: fileExtension) else {
            throw Error.fileNotFound
        }
        let data = try fileReader.read(fileURL: fileURL)
        let jsonObjects = try JSONDecoder().decode([LocationJsonObject].self, from: data)
        return try jsonObjects.map({ try Location(jsonObject: $0) })
    }
}
