//
//  LocationFiltering.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

protocol LocationFiltering: class {
    func filter(locations: [Location]) -> (included: [Location], excluded: [Location])
}
