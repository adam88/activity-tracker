//
//  GPSFilter.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

class GPSFilter: LocationFiltering {
    let minAccuracy: Double // in meters
    let minDistance: Double // in meters
    let minTimestamp: Double // in seconds
    
    init(minAccuracy: Double = 20.0, minDistance: Double = 1.0, minTimestamp: Double = 1.0) {
        self.minAccuracy = minAccuracy
        self.minDistance = minDistance
        self.minTimestamp = minTimestamp
    }
    
    func filter(locations: [Location]) -> (included: [Location], excluded: [Location]) {
        print("GPSFilter - filter")
        var included = [Location]()
        var excluded = [Location]()
        for location in locations {
            if location.accuracy > 0.0 && location.accuracy <= minAccuracy {
                if let previousLocation = included.last {
                    if location.clLocation.distance(from: previousLocation.clLocation) >= minDistance {
                        if location.timestamp - previousLocation.timestamp >= minTimestamp {
                            included.append(location)
                        } else {
                            excluded.append(location)
                        }
                    } else {
                        excluded.append(location)
                    }
                } else {
                    included.append(location)
                }
            } else {
                excluded.append(location)
            }
        }
        return (included: included, excluded: excluded)
    }
}
