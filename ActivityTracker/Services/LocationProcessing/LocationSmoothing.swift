//
//  LocationSmoothing.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

protocol LocationSmoothing: class {
    func smooth(locations: [Location]) -> [Location]
}
