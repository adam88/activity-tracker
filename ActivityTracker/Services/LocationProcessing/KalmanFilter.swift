//
//  KalmanFilter.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

class KalmanFilter: LocationSmoothing {
    let decay: Double = 3.0
    let minAccuracy: Double = 1.0
    var variance: Double = -1.0
    var timestamp: Double = 0.0 // in milliseconds
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    func smooth(locations: [Location]) -> [Location] {
        print("KalmanFilter - smooth")
        return locations.map({ process(location: $0) })
    }
    
    private func process(location: Location) -> Location {
        let lat = location.latitude
        let lng = location.longitude
        let accuracy = location.accuracy < self.minAccuracy ? self.minAccuracy : location.accuracy
        let timestamp = location.timestamp * 1000.0
        
        if (self.variance < 0) {
            self.timestamp = timestamp
            self.lat = location.latitude
            self.lng = lng
            self.variance = accuracy * accuracy
        } else {
            let timeIncMs = timestamp - self.timestamp
            
            if (timeIncMs > 0) {
                self.variance += (timeIncMs * self.decay * self.decay) / 1000.0
                self.timestamp = timestamp
            }
            
            let _k = self.variance / (self.variance + (accuracy * accuracy))
            self.lat += _k * (lat - self.lat)
            self.lng += _k * (lng - self.lng)
            
            self.variance = (1 - _k) * self.variance
        }
        
        return Location(latitude: self.lat,
                        longitude: self.lng,
                        altitude: location.altitude,
                        accuracy: location.accuracy,
                        distance: location.distance,
                        timestamp: location.timestamp)
    }
}
