//
//  Location.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

struct Location {
    let latitude: Double
    let longitude: Double
    let altitude: Double
    let accuracy: Double
    let distance: Double
    let timestamp: Double
    
    init(latitude: Double, longitude: Double, altitude: Double, accuracy: Double, distance: Double, timestamp: Double) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.accuracy = accuracy
        self.distance = distance
        self.timestamp = timestamp
    }
    
    init(jsonObject: LocationJsonObject) throws {
        self.latitude = try jsonObject.latitude.toDouble()
        self.longitude = try jsonObject.longitude.toDouble()
        self.altitude = try jsonObject.altitude.toDouble()
        self.accuracy = try jsonObject.accuracy.toDouble()
        self.distance = try jsonObject.distance.toDouble()
        self.timestamp = try jsonObject.timestamp.toDouble()
    }
}

extension Location: Equatable {
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.latitude == rhs.latitude
            && lhs.longitude == rhs.longitude
            && lhs.altitude == rhs.altitude
            && lhs.accuracy == rhs.accuracy
            && lhs.distance == rhs.distance
            && lhs.timestamp == rhs.timestamp
    }
}

extension Location: CustomStringConvertible {
    var description: String {
        return "latitude: \(latitude), longitude: \(longitude), altitude: \(altitude), accuracy: \(accuracy), distance: \(distance), timestamp: \(timestamp)"
    }
}
