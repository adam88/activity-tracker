//
//  LocationJsonObject.swift
//  ActivityTracker
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import Foundation

struct LocationJsonObject: Decodable {
    let latitude: String
    let longitude: String
    let altitude: String
    let accuracy: String
    let distance: String
    let timestamp: String
}
