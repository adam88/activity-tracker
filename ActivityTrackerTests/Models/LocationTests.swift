//
//  LocationTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class LocationTests: XCTestCase {
    func testCreateLocationFromLocactionJsonObject() throws {
        let latitude: Double = 10.0
        let longitude: Double = 20.0
        let altitude: Double = 30.0
        let accuracy: Double = 40.0
        let distance: Double = 50.0
        let timestamp: Double = 60.0
        
        let jsonObject = LocationJsonObject(latitude: "\(latitude)",
                                            longitude: "\(longitude)",
                                            altitude: "\(altitude)",
                                            accuracy: "\(accuracy)",
                                            distance: "\(distance)",
                                            timestamp: "\(timestamp)")
        
        let location = try Location(jsonObject: jsonObject)
        
        XCTAssertEqual(location.latitude, latitude)
        XCTAssertEqual(location.longitude, longitude)
        XCTAssertEqual(location.altitude, altitude)
        XCTAssertEqual(location.accuracy, accuracy)
        XCTAssertEqual(location.distance, distance)
        XCTAssertEqual(location.timestamp, timestamp)
    }
}
