//
//  LocationJsonObjectTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class LocationJsonObjectTests: XCTestCase {
    func testDecodeLocationJsonObject() throws {
        let json: [String: String] = [
            "longitude": "19.315773",
            "altitude": "175.162534",
            "timestamp": "0",
            "latitude": "51.893638",
            "accuracy": "9.864261",
            "distance": "0.000"
        ]
        let data = try JSONEncoder().encode(json)
        
        let location = try JSONDecoder().decode(LocationJsonObject.self, from: data)
        
        XCTAssertEqual(location.longitude, json["longitude"])
        XCTAssertEqual(location.altitude, json["altitude"])
        XCTAssertEqual(location.timestamp, json["timestamp"])
        XCTAssertEqual(location.latitude, json["latitude"])
        XCTAssertEqual(location.accuracy, json["accuracy"])
        XCTAssertEqual(location.distance, json["distance"])
    }
}
