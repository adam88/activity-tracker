//
//  String+ConversionTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class StringConversionTests: XCTestCase {
    func testToDoubleThrowsError() throws {
        XCTAssertThrowsError(try "abc".toDouble())
    }
    
    func testToDoubleReturnsValue() throws {
        XCTAssertEqual(try "100.50".toDouble(), 100.50)
    }
}
