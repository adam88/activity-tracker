//
//  Location+CLTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class LocationCLTests: XCTestCase {
    let location = Location(latitude: 51.894594,
                        longitude: 19.315904,
                        altitude: 175.452029,
                        accuracy: 10.839159,
                        distance: 0.034,
                        timestamp: 14.0)
    
    func testConvertToCLLocation() {
        let clLocation = location.clLocation
        
        XCTAssertEqual(location.latitude, clLocation.coordinate.latitude)
        XCTAssertEqual(location.longitude, clLocation.coordinate.longitude)
        XCTAssertEqual(location.altitude, clLocation.altitude)
        XCTAssertEqual(location.accuracy, clLocation.horizontalAccuracy)
    }
    
    func testConvertToCLLocationCoordinate2D() {
        let clCoordinate = location.clCoordinate
        
        XCTAssertEqual(location.latitude, clCoordinate.latitude)
        XCTAssertEqual(location.longitude, clCoordinate.longitude)
    }
}
