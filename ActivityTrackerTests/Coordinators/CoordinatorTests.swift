//
//  CoordinatorTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class CoordinatorTests: XCTestCase {
    class ParentCoordinator: Coordinator {
        var childCoordinators: [Coordinator] = []
        weak var delegate: CoordinatorDelegate?
    
        func start() { }
        
        func finish() { }
    }
    
    class ChildCoordinator: Coordinator {
        var childCoordinators: [Coordinator] = []
        weak var delegate: CoordinatorDelegate?
    
        func start() { }
        
        func finish() { }
    }
    
    func testAddChildCoordinator() {
        let parentCoordinator = ParentCoordinator()
        let childCoordinator = ChildCoordinator()
        
        parentCoordinator.addChildCoordinator(childCoordinator)
        
        XCTAssertEqual(parentCoordinator.childCoordinators.count, 1)
    }
    
    func testRemoveChildCoordinator() {
        let parentCoordinator = ParentCoordinator()
        let childCoordinator = ChildCoordinator()
        
        parentCoordinator.addChildCoordinator(childCoordinator)
        parentCoordinator.removeChildCoordinator(childCoordinator)
        
        XCTAssertEqual(parentCoordinator.childCoordinators.count, 0)
    }
    
    func testRemoveAllChildCoordinatorsWithType() {
        let parentCoordinator = ParentCoordinator()
        let firstChildCoordinator = ChildCoordinator()
        let secondChildCoordinator = ChildCoordinator()
        
        parentCoordinator.addChildCoordinator(firstChildCoordinator)
        parentCoordinator.addChildCoordinator(secondChildCoordinator)
        parentCoordinator.removeAllChildCoordinatorsWithType(ChildCoordinator.self)
        
        XCTAssertEqual(parentCoordinator.childCoordinators.count, 0)
    }
    
    func testRemoveAllChildCoordinators() {
        let parentCoordinator = ParentCoordinator()
        let firstChildCoordinator = ChildCoordinator()
        let secondChildCoordinator = ChildCoordinator()
        
        parentCoordinator.addChildCoordinator(firstChildCoordinator)
        parentCoordinator.addChildCoordinator(secondChildCoordinator)
        parentCoordinator.removeAllChildCoordinators()
        
        XCTAssertEqual(parentCoordinator.childCoordinators.count, 0)
    }
}
