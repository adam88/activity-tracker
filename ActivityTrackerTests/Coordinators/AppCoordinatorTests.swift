//
//  AppCoordinatorTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 19/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class AppCoordinatorTests: XCTestCase {
    func testAppCoordinatorContainsTrackerCoordinator() {
        let coordinator = AppCoordinator(window: UIWindow())
        
        coordinator.start()
        
        XCTAssertNotNil(coordinator.childCoordinators.first(where: { $0 is TrackerCoordinator }))
    }
    
    func testAppCoordinatorShowsTrackerView() {
        let coordinator = AppCoordinator(window: UIWindow())
        
        coordinator.start()
        
        XCTAssertNotNil((coordinator.window?.rootViewController as? UINavigationController)?.viewControllers.first as? TrackerViewController)
    }
}
