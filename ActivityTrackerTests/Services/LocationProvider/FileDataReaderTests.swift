//
//  FileDataReaderTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class FileDataReaderTests: XCTestCase {
    func testReadReturnsData() throws {
        let url = Bundle.main.url(forResource: "path", withExtension: "json")!
        let reader = FileDataReader()

        XCTAssertNoThrow(try reader.read(fileURL: url))
    }
}
