//
//  LocationFileDataProviderTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 20/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class LocationFileDataProviderTests: XCTestCase {
    func testGetLocationsFromDataProvider() throws {
        let dataProvider = LocationFileDataProvider(dataReader: FileDataReader())
        
        let locations = try dataProvider.getLocations()
        
        XCTAssert(locations.count > 0)
    }
    
    func testPerformanceGetLocationsFromDataProvider() throws {
        measure {
            let _ = try? LocationFileDataProvider(dataReader: FileDataReader()).getLocations()
        }
    }
}
