//
//  GPSFilterTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class GPSFilterTests: XCTestCase {
    func testMinAccuracyCondition() {
        let loc1 = Location(latitude: 51.894594,
                            longitude: 19.315904,
                            altitude: 175.452029,
                            accuracy: 10.839159,
                            distance: 0.034,
                            timestamp: 14.0)
        
        let loc2 = Location(latitude: 51.893597,
                            longitude: 19.31591,
                            altitude: 175.439102,
                            accuracy: 30.31883,
                            distance: 0.035,
                            timestamp: 15.0)
        
        let filter = GPSFilter(minAccuracy: 20.0, minDistance: 1.0, minTimestamp: 1.0)
        
        let filtredLocations = filter.filter(locations: [loc1, loc2])
        
        XCTAssertTrue(loc1.accuracy <= filter.minAccuracy)
        XCTAssertFalse(loc2.accuracy <= filter.minAccuracy)
        XCTAssertTrue(loc2.clLocation.distance(from: loc1.clLocation) >= filter.minDistance)
        XCTAssertTrue(loc2.timestamp - loc1.timestamp >= filter.minTimestamp)
        XCTAssertTrue(filtredLocations.included.contains(loc1))
        XCTAssertEqual(filtredLocations.included.count, 1)
        XCTAssertTrue(filtredLocations.excluded.contains(loc2))
        XCTAssertEqual(filtredLocations.excluded.count, 1)
    }
    
    func testMinDistanceCondition() {
        let loc1 = Location(latitude: 51.893594,
                            longitude: 19.315904,
                            altitude: 175.452029,
                            accuracy: 10.839159,
                            distance: 0.034,
                            timestamp: 14.0)
        
        let loc2 = Location(latitude: 51.893597,
                            longitude: 19.31591,
                            altitude: 175.439102,
                            accuracy: 9.31883,
                            distance: 0.035,
                            timestamp: 15.0)
        
        let filter = GPSFilter(minAccuracy: 20.0, minDistance: 1.0, minTimestamp: 1.0)
        
        let filtredLocations = filter.filter(locations: [loc1, loc2])
        
        XCTAssertTrue(loc1.accuracy <= filter.minAccuracy)
        XCTAssertTrue(loc2.accuracy <= filter.minAccuracy)
        XCTAssertFalse(loc2.clLocation.distance(from: loc1.clLocation) >= filter.minDistance)
        XCTAssertTrue(loc2.timestamp - loc1.timestamp >= filter.minTimestamp)
        XCTAssertTrue(filtredLocations.included.contains(loc1))
        XCTAssertEqual(filtredLocations.included.count, 1)
        XCTAssertTrue(filtredLocations.excluded.contains(loc2))
        XCTAssertEqual(filtredLocations.excluded.count, 1)
    }
    
    func testMinTimestampCondition() {
        let loc1 = Location(latitude: 51.894594,
                            longitude: 19.315904,
                            altitude: 175.452029,
                            accuracy: 10.839159,
                            distance: 0.034,
                            timestamp: 14.0)
        
        let loc2 = Location(latitude: 51.893597,
                            longitude: 19.31591,
                            altitude: 175.439102,
                            accuracy: 9.31883,
                            distance: 0.035,
                            timestamp: 14.5)
        
        let filter = GPSFilter(minAccuracy: 20.0, minDistance: 1.0, minTimestamp: 1.0)
        
        let filtredLocations = filter.filter(locations: [loc1, loc2])
        
        XCTAssertTrue(loc1.accuracy <= filter.minAccuracy)
        XCTAssertTrue(loc2.accuracy <= filter.minAccuracy)
        XCTAssertTrue(loc2.clLocation.distance(from: loc1.clLocation) >= filter.minDistance)
        XCTAssertFalse(loc2.timestamp - loc1.timestamp >= filter.minTimestamp)
        XCTAssertTrue(filtredLocations.included.contains(loc1))
        XCTAssertEqual(filtredLocations.included.count, 1)
        XCTAssertTrue(filtredLocations.excluded.contains(loc2))
        XCTAssertEqual(filtredLocations.excluded.count, 1)
    }
}
