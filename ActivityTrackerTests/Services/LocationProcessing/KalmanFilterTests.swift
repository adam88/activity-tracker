//
//  KalmanFilterTests.swift
//  ActivityTrackerTests
//
//  Created by Adam Gronczewski on 21/02/2021.
//  Copyright © 2021 AGSoftware. All rights reserved.
//

import XCTest
@testable import ActivityTracker

class KalmanFilterTests: XCTestCase {
    let json =
        """
        [{"longitude":"19.315773","altitude":"175.162534","timestamp":"0","latitude":"51.893638","accuracy":"9.864261","distance":"0.000"},{"latitude":"51.893660","timestamp":"0","longitude":"19.315780","accuracy":"9.405735","distance":"0.002","altitude":"175.273693"},{"longitude":"19.315771","altitude":"175.512192","accuracy":"7.280545","latitude":"51.893666","distance":"0.003","timestamp":"1"},{"longitude":"19.315770","distance":"0.006","accuracy":"6.878234","altitude":"175.696232","timestamp":"2","latitude":"51.893688"},{"altitude":"175.893443","accuracy":"11.121818","longitude":"19.315731","timestamp":"3","latitude":"51.893681","distance":"0.009"},{"latitude":"51.893677","accuracy":"14.424778","altitude":"175.976907","distance":"0.010","longitude":"19.315712","timestamp":"4"},{"distance":"0.013","longitude":"19.315713","altitude":"176.016307","timestamp":"5","accuracy":"10.714760","latitude":"51.893652"},{"longitude":"19.315752","accuracy":"6.751239","timestamp":"6","altitude":"176.007548","distance":"0.016","latitude":"51.893655"},{"timestamp":"7","altitude":"176.008832","longitude":"19.315762","accuracy":"8.279055","distance":"0.021","latitude":"51.893610"},{"longitude":"19.315762","accuracy":"8.279055","timestamp":"8","altitude":"176.008832","latitude":"51.893610","distance":"0.021"},{"latitude":"51.893584","longitude":"19.315811","timestamp":"9","altitude":"175.899532","accuracy":"6.684282","distance":"0.025"},{"longitude":"19.315837","altitude":"175.798285","timestamp":"10","latitude":"51.893563","accuracy":"7.699458","distance":"0.028"},{"latitude":"51.893566","timestamp":"11","altitude":"175.648484","accuracy":"8.603286","distance":"0.029","longitude":"19.315846"},{"latitude":"51.893582","altitude":"175.551020","timestamp":"12","accuracy":"9.119011","distance":"0.031","longitude":"19.315869"},{"timestamp":"13","latitude":"51.893591","distance":"0.032","accuracy":"7.957371","longitude":"19.315876","altitude":"175.474512"},{"distance":"0.034","accuracy":"10.839159","latitude":"51.893594","longitude":"19.315904","altitude":"175.452029","timestamp":"14"},{"timestamp":"15","altitude":"175.439102","latitude":"51.893597","distance":"0.035","accuracy":"9.318830","longitude":"19.315910"},{"timestamp":"16","latitude":"51.893596","distance":"0.036","altitude":"175.448722","longitude":"19.315926","accuracy":"6.652814"},{"longitude":"19.315929","accuracy":"8.267003","altitude":"175.441368","latitude":"51.893597","timestamp":"17","distance":"0.036"},{"longitude":"19.315918","altitude":"175.446325","accuracy":"12.278127","timestamp":"18","distance":"0.037","latitude":"51.893599"},{"accuracy":"9.816942","altitude":"175.432590","latitude":"51.893599","longitude":"19.315897","timestamp":"19","distance":"0.038"},{"altitude":"175.453509","accuracy":"6.581202","latitude":"51.893616","distance":"0.040","longitude":"19.315891","timestamp":"20"}]
        """

    func testKalmanFilterReturnsLocations() throws {
        let jsonObjects = try JSONDecoder().decode([LocationJsonObject].self, from: json.data(using: .utf8)!)
        let locations = try jsonObjects.map({ try Location(jsonObject: $0) })
        let filter = KalmanFilter()
        
        let filtredLocations = filter.smooth(locations: locations)
    
        XCTAssertFalse(filtredLocations.isEmpty)
        XCTAssertEqual(locations.count, filtredLocations.count)
        XCTAssertNotEqual(locations, filtredLocations)
    }
}
